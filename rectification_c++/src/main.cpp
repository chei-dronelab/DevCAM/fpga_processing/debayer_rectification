#include <vector>
#include <algorithm>
#include <climits>
#include "lookup_table.hpp"
#include "functions.hpp"
#include <float.h>
#include <assert.h>
#include <set>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <chrono>

using namespace std;
using namespace cv;
// using json = nlohmann::json;
int main(int argc, char *argv[])
{
    /*
    Reads in the input (RGB) image, compressed Look Up Tables, converts image to bayer, and performs debayering and rectification
    */
    // string falloff_function[] = {"exp","linear"};
    // string distance_metric[] = {"l1","l2"};
    // float distance_power = 2.0;
    /*
    Usage: ./test <input_image_path> <compressed_LUTx_path> <compressed_LUTy_path> <output_path> <decay_f_power> <number_of_nearest_neighbours_to_splat> <nn_dist_metric> <decay_func_exp>
    int decay_f_power = 2, int nn = 8, string nn_dist_metric = "l2", int decay_func_exp = 1
    */
    std::string input_image_path;
    std::string compressed_LUTx_path;
    std::string compressed_LUTy_path;
    std::string output_path;
    int decay_f_power = 2;
    int n_nearest_neighbours = 8;
    string distance_metric = "l2";
    int decay_func_exp = 1;
    int max_buffer_width;
    bool are_LUTs_compressed = true;

    process_args(argc, argv, input_image_path, output_path, compressed_LUTx_path, compressed_LUTy_path,
                    decay_f_power, n_nearest_neighbours, distance_metric, decay_func_exp, are_LUTs_compressed, max_buffer_width);
    Mat img = imread(input_image_path.c_str(), IMREAD_COLOR);
    vec3<float> img_vector;
    /*
    Converts image in Mat format to a 3D vector (mxnx3), since we do processing using vectors
    */
    for (int i = 0; i < img.rows; ++i)
    {
        vector<vector<float>> row(img.cols, vector<float>(3));
        for (int j = 0; j < img.cols; ++j)
        {
            Vec3b pxl = img.at<Vec3b>(i, j);
            for (int k = 0; k < 3; k++)
            {
                // vec3b stores BGR
                row[j][2 - k] = float((int)pxl(k));
            }
        }
        img_vector.push_back(row);
    }

    /*
    Read compressed Lookup Tables, and initialise the class LUT (defined in lookup_table.cpp) based on it.
    That class basically provides a wrapper function to get pixel locations in the output image when you give it the input pixel coordinates, which it does by LUT decompression,
    */
    vector<vector<float>> LUTx = fileToVector(compressed_LUTx_path.c_str());
    vector<vector<float>> LUTy = fileToVector(compressed_LUTy_path.c_str());


    cout << "LUTx size: " << LUTx.size() << " " << LUTx[0].size() << "\n";
    cout << "LUTy size: " << LUTy.size() << " " << LUTy[0].size() << "\n";

    class LUT tables(are_LUTs_compressed, LUTx, LUTy);
    // class LUT tables(H,make_tuple(img_vector.size(),img_vector[0].size()));

    tuple<int, int> output_image_size = make_tuple(img.cols, img.rows);

    cout << "Output Image size: " << get<1>(output_image_size) << " " << get<0>(output_image_size) << "\n";

    int channel[] = {0, 1, 1, 2};

    /*
    Applying the bayer filter on our image. Even if we read a bayered image from file, this is fine,
    since opencv reads grayscale image as RGB with all channels same.
    */
    vector<vector<float>> source_image_bayer(img_vector.size(), vector<float>(img_vector[0].size()));
    for (int y = 0; y < img_vector.size(); y++)
    {
        for (int x = 0; x < img_vector[0].size(); x++)
        {
            int c = channel[(1 - x % 2) * 2 + (1 - y % 2)];
            source_image_bayer[y][x] = img_vector[y][x][c];
        }
    }
    vec3<float> output_stream(3, vec2<float>(get<1>(output_image_size), vector<float>(get<0>(output_image_size))));
    vec3<float> vis_buffer(3, vec2<float>(get<1>(output_image_size), vector<float>(get<0>(output_image_size))));
    vec3<float> vis_sec_buffer(3, vec2<float>(get<1>(output_image_size), vector<float>(get<0>(output_image_size))));

    // vec3<float> output_stream(3, vec2<float> (source_image_bayer.size(), vector<float> (source_image_bayer[0].size())));

    cout << "Source Image bayer, size: " << source_image_bayer.size() << " " << source_image_bayer[0].size() << "\n";

    /* Writing the color image (in the 3D vector format), and the bayered image to files.
    This provides a handy way to check if writetofile and vectorization and bayering works or not
    */
    // writetofile(source_image_bayer,"../imgs/output/source_image_bayer.png");
    // writetofile(img_vector,"../imgs/output/source_image_color_reconstructed.png");

    auto start = std::chrono::high_resolution_clock::now();

    /* the main function, doing debayering and rectification.
    takes in the bayer pattern which we used in bayering, the bayered image, and the LUT object as inputs
    writes to output_stream
    */
    debayer_rectify(BAYER_BGGR, source_image_bayer, tables, output_stream, output_image_size, vis_buffer, 
                    vis_sec_buffer, decay_f_power, n_nearest_neighbours, distance_metric, decay_func_exp, max_buffer_width);

    auto stop = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(stop - start);

    cout << "Time taken to run: " << duration.count() << " seconds \n";

    writetofile2(output_stream, output_path.c_str());

    writetofile2(vis_buffer, "primary_buffer.exr");
    writetofile2(vis_sec_buffer, "secondary_buffer.exr");

    return 0;
}