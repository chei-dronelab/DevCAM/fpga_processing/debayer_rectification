#!/bin/bash
# Usage: run_code.sh <input_image_path> <compressed_LUTx path> <compressed_LUTy_path> <output_image_path>
echo "Starting compiling"
g++ -o output json.hpp lookup_table.cpp functions.cpp main.cpp --std=c++11 $(pkg-config --cflags --libs opencv) -O3
echo "Done compiling"
