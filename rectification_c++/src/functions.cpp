#include <vector>
#include <algorithm>
#include <climits>
#include "lookup_table.hpp"
#include "functions.hpp"
#include "json.hpp"

#include <float.h>
#include <assert.h>
#include <set>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <chrono>
#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include <unistd.h>

#define min(a, b) a < b ? a : b

// using namespace std::chrono;
using namespace std;
using namespace cv;
using json = nlohmann::json;

void norm(vec2<float> &image)
{
    float maxval = FLT_MIN, minval = FLT_MAX;
    for (uint i = 0; i < image.size(); i++)
    {
        float maxelem = FLT_MIN, minelem = FLT_MAX;
        for (uint j = 0; j < image[0].size(); j++)
        {
            maxelem = maxelem > image[i][j] ? maxelem : image[i][j];
            maxelem = minelem < image[i][j] ? minelem : image[i][j];
        }
        // float f = *(max_element(image[i]));
        // float m = *(min_element(image[i]));
        maxval = minelem > maxval ? maxelem : maxval;
        minval = minelem < minval ? minelem : minval;
    }
    int l = image[0].size();
    for (uint i = 0; i < image.size(); i++)
    {
        for (uint j = 0; j < l; j++)
        {
            image[i][j] = (image[i][j] - minval) / (maxval - minval);
        }
    }
}

float distance(float x1, float y1, float x2, float y2, string metric = "l1")
{
    if (metric == "l1")
    {
        return abs(x1 - x2) + abs(y1 - y2);
    }
    if (metric == "l2")
    {
        return sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2));
    }
}
vector<tuple<float, int, int>> n_nearest_neighbours(float x, float y, int n = 8, string metric = "l2")
{
    // Takes a point in 2D space,
    // returns n nearest neighbours (n<=15)
    vector<tuple<float, int, int>> nn;
    int xint = int(x), yint = int(y);
    float fracx = float(xint) > x ? (1 - float(xint) + x) : (x - float(xint));
    float fracy = float(yint) > y ? (1 - float(yint) + y) : (y - float(yint));
    int window_size = int(ceil(sqrt(n)));
    int w2 = window_size / 2;
    int xmin = (xint - w2), xmax = (xint + w2);
    int ymin = (yint - w2), ymax = (yint + w2);
    if (window_size % 2 == 0)
    {
        if (fracx < 0.5)
        {
            xmin++;
        }
        else
        {
            xmax--;
        }
        if (fracy < 0.5)
        {
            ymin++;
        }
        else
        {
            ymax--;
        }
    }
    for (int xn = xmin; xn <= xmax; xn++)
    {
        for (int yn = ymin; yn <= ymax; yn++)
        {
            float a = distance(x, y, xn, yn, metric);
            nn.push_back(make_tuple(a, xn, yn));
        }
    }

    return nn;
}

void debayer_rectify(BAYER bayer_pattern, vec2<float> source_image_bayer, class LUT &tables, vec3<float> &output_stream, 
                     tuple<int, int> output_image_size, vec3<float> &vis_buffer, vec3<float> &vis_sec_buffer,
                     int decay_f_power = 2, int nn = 8, string nn_dist_metric = "l2", int decay_func_exp = 1, int op_buffer_width = 60)
{
    
    // static int op_buffer_width = 60; // minimum number of rows so that number of out of bound rows is 0
    static int safe_interpolation_buffer_margin = 4;
    vector<vector<vector<float>>> output_buffer(3, vec2<float>(op_buffer_width, vector<float>(get<0>(output_image_size), 0.0)));
    vec2<float> input_buffer(source_image_bayer.size(), vector<float>(source_image_bayer[0].size(), 0.0));
    vector<vector<vector<float>>> secondary_buffer2(3, vec2<float>(op_buffer_width, vector<float>(get<0>(output_image_size), 0.0)));

    int shape_1 = output_stream.size(), shape_2 = output_stream[0].size(), shape_3 = output_stream[0][0].size();

    float debug_max_output_buffer_width = 0, db_op_top = -10000, db_op_bottom = 10000, debug_min_output_buffer_width = 10000;
    float max_inrows_before_popping = 0, max_outrows_popped_together = 0, input_row_counter = 0, output_row_counter = 0;
    float debug_max_temporary_buffer_width = 0, db_temp_top = -10000, db_temp_bottom = 10000;

    vector<bool> did_anything_land_here(op_buffer_width, false);
    vector<int> channel_dict;
    switch (bayer_pattern)
    {
    case BAYER_BGGR:
    {
        channel_dict = {0, 1, 1, 2};
        break;
    }
    case BAYER_GRBG:
    {
        channel_dict = {1, 0, 2, 1};
        break;
    }
    case BAYER_GBRG:
    {
        channel_dict = {1, 2, 0, 1};
        break;
    }
    case BAYER_RGGB:
    {
        channel_dict = {0, 1, 1, 2};
        break;
    }
    default:
    {
        channel_dict = {0, 1, 1, 2};
        break;
    }
    }

    int miny = 0, maxy = op_buffer_width - safe_interpolation_buffer_margin, output_buffer_offset = 1;
    float xind, yind;
    float intensity;
    int channel;
    int input_buffer_calls = 0;
    float yidx;
    int out_of_bound_rows = 0;
    bool kickstart = false;

    for (int yin = 0; yin < source_image_bayer.size(); yin++)
    {
        bool row_touched = false, need_row = false;
        float allmaxy = FLT_MIN;
        float allminy = FLT_MAX;
        input_buffer_calls = 0;
        long aflag_count = 0;
        long total_counts = 0;
        vector<int> how_many_pixels_landed_here(op_buffer_width, 0);
        for (int xin = 0; xin < source_image_bayer[0].size(); xin++)
        {

            tuple<float, float> outpoints = tables.LUT_fwdf2point(yin, xin);
            float x = get<0>(outpoints);
            float y = get<1>(outpoints);

            int channel = channel_dict[(1 - xin % 2) * 2 + (1 - yin % 2)]; // check: should be equivalent to (for integer xin and yin): (!xin&1)<<1 + yin&1

            intensity = source_image_bayer[yin][xin];
            if (x < 0 || x >= get<0>(output_image_size) || y < 0 || y >= get<1>(output_image_size))
            {
                continue;
            }
            need_row = true;
            allminy = min(allminy, y);
            allmaxy = max(allmaxy, y);
            debug_max_output_buffer_width = max(debug_max_output_buffer_width, max(abs(allmaxy - yin), abs(yin - allminy)));
            debug_min_output_buffer_width = min(debug_min_output_buffer_width, max(abs(allmaxy - yin), abs(yin - allminy)));
            // int decay_f_power = 2, int nn = 8, string nn_dist_metric = "l2", int decay_func_exp = 1
            // vector <tuple <float,int,int>> nbrs=n_nearest_neighbours(y, x, nn, "l2");
            vector<tuple<float, int, int>> nbrs = n_nearest_neighbours(y, x, nn, nn_dist_metric);
            for (int nbrs_idx = 0; nbrs_idx < nbrs.size(); nbrs_idx++)
            {
                int yn = get<1>(nbrs[nbrs_idx]), xn = get<2>(nbrs[nbrs_idx]);
                total_counts += 1;
                if (yn < miny || yn > (miny + op_buffer_width) || yn >= get<1>(output_image_size) || xn < 0 || xn >= get<0>(output_image_size))
                {
                    // cout<<yin<<">"<<yn<<":"<<miny<<"\n";
                    aflag_count += 1;
                    continue;
                }
                else
                {
                    how_many_pixels_landed_here[int(yn) % op_buffer_width] += 1;
                    did_anything_land_here[int(yn) % op_buffer_width] = true;

                    float w = 0.0;
                    // if(decay_func_exp == 1){
                    w = exp(-1 * pow(get<0>(nbrs[nbrs_idx]), decay_f_power));
                    // }
                    // else{
                    // w = 1.0 / (1+ pow(get<0>(nbrs[nbrs_idx]),decay_f_power));
                    // }

                    output_buffer[channel][int(yn) % op_buffer_width][int(xn)] += intensity * w;
                    secondary_buffer2[channel][int(yn) % op_buffer_width][int(xn)] += w;
                }
            }
        }
        // cout<<"yin: "<<yin<<" "<<aflag_count<<" "<<miny<<"\n";
        bool aflag = aflag_count > total_counts * 0.9;
        if (aflag)
        {
            // need to find the smallest buffer size such that out_of_bound_rows = 0
            out_of_bound_rows += 1;
        }

        if (how_many_pixels_landed_here[miny % op_buffer_width] == 0 && kickstart)
        {
            int ycoord = miny % op_buffer_width;
            // testing vis_buffer and vis_sec_buffer
            for (int i = 0; i < shape_1; i++)
            {
                vis_buffer[i][miny] = output_buffer[i][ycoord];
                vis_sec_buffer[i][miny] = secondary_buffer2[i][ycoord];
            }

            for (int i = 0; i < shape_1; i++)
            {
                for (int k = 0; k < shape_3; k++)
                {
                    output_buffer[i][ycoord][k] = secondary_buffer2[i][ycoord][k] == 0.0 ? 0.0 : (output_buffer[i][ycoord][k] / secondary_buffer2[i][ycoord][k]);
                }
            }
            // equivalent to:
            // output_image2[:,miny,:]=np.nan_to_num(np.divide(output_image2[:,miny,:],secondary_buffer[:,miny,:]))
            // output_buffer[:,ycoord,:]=np.nan_to_num(np.divide(output_buffer[:,ycoord,:],secondary_buffer2[:,ycoord,:]))

            for (int i = 0; i < shape_1; i++)
            {
                output_stream[i][miny] = output_buffer[i][ycoord];
            }

            // clearing popped rows
            for (int i = 0; i < shape_1; i++)
            {
                for (int j = 0; j < shape_3; j++)
                {
                    output_buffer[i][ycoord][j] = 0.0;
                    secondary_buffer2[i][ycoord][j] = 0.0;
                }
            }
            how_many_pixels_landed_here[miny % op_buffer_width] = 0;
            did_anything_land_here[miny % op_buffer_width] = false;
            miny++;
        }
        if (how_many_pixels_landed_here[miny % op_buffer_width] != 0)
        {
            kickstart = true;
        }
    }
    // cleaning out any rows left
    int start_pt = miny;
    int end_pt = min(miny + op_buffer_width, output_stream[0].size());
    while (miny < end_pt)
    {
        int ycoord = miny % op_buffer_width;
        // testing vis_buffer and vis_sec_buffer
        for (int i = 0; i < shape_1; i++)
        {
            vis_buffer[i][miny] = output_buffer[i][ycoord];
            vis_sec_buffer[i][miny] = secondary_buffer2[i][ycoord];
        }

        for (int i = 0; i < shape_1; i++)
        {
            for (int k = 0; k < shape_3; k++)
            {
                output_buffer[i][ycoord][k] = secondary_buffer2[i][ycoord][k] == 0.0 ? 0.0 : (output_buffer[i][ycoord][k] / secondary_buffer2[i][ycoord][k]);
            }
        }
        // equivalent to:
        // output_image2[:,miny,:]=np.nan_to_num(np.divide(output_image2[:,miny,:],secondary_buffer[:,miny,:]))
        // output_buffer[:,ycoord,:]=np.nan_to_num(np.divide(output_buffer[:,ycoord,:],secondary_buffer2[:,ycoord,:]))

        for (int i = 0; i < shape_1; i++)
        {
            output_stream[i][miny] = output_buffer[i][ycoord];
        }

        // clearing popped rows
        for (int i = 0; i < shape_1; i++)
        {
            for (int j = 0; j < shape_3; j++)
            {
                output_buffer[i][ycoord][j] = 0.0;
                secondary_buffer2[i][ycoord][j] = 0.0;
            }
        }
        // how_many_pixels_landed_here[miny%op_buffer_width]=0;
        // did_anything_land_here[miny%op_buffer_width]=false;
        miny++;
    }
    cout << "Number of out of bound rows:" << out_of_bound_rows << "\n";
    cout << "miny: " << miny << "\n";
    return;
}

vector<vector<float>> fileToVector(const char *name)
{
    vector<vector<float>> result;
    ifstream input(name);
    if(!input.good()){
        cout<<"File "<<name<<" not good. Exiting\n";
        exit(0);
    }
    string lineData;
    cout << "reading data from " << name << "\n";
    int rows_read=0;
    while (getline(input, lineData))
    {
        float d;
        vector<float> row;
        stringstream lineStream(lineData);

        while (lineStream >> d)
            row.push_back(d);

        result.push_back(row);
        rows_read++;
    }
    cout<<"Rows read: "<<rows_read<<"\n";
    return result;
}

void writetofile(vec2<float> &gray, const char *name)
{
    Mat matAngles(gray.size(), gray.at(0).size(), CV_32FC1);
    for (int i = 0; i < matAngles.rows; ++i)
    {
        for (int j = 0; j < matAngles.cols; ++j)
        {
            matAngles.at<float>(i, j) = gray.at(i).at(j);
        }
    }
    imwrite(name, matAngles);
}
void writetofile(vec3<float> &colorimages, const char *name)
{
    /*
        Overloaded to handle color images, if they are of shape mXnX3
    */

    vector<Mat> array_to_merge;

    Mat R(colorimages.size(), colorimages.at(0).size(), CV_32FC1);
    for (int i = 0; i < R.rows; ++i)
    {
        for (int j = 0; j < R.cols; ++j)
        {
            R.at<float>(i, j) = colorimages.at(i).at(j).at(0);
        }
    }
    Mat G(colorimages.size(), colorimages.at(0).size(), CV_32FC1);
    for (int i = 0; i < G.rows; ++i)
    {
        for (int j = 0; j < G.cols; ++j)
        {
            G.at<float>(i, j) = colorimages.at(i).at(j).at(1);
        }
    }

    Mat B(colorimages.size(), colorimages.at(0).size(), CV_32FC1);
    for (int i = 0; i < B.rows; ++i)
    {
        for (int j = 0; j < B.cols; ++j)
        {
            B.at<float>(i, j) = colorimages.at(i).at(j).at(2);
        }
    }

    array_to_merge.push_back(B);
    array_to_merge.push_back(G);
    array_to_merge.push_back(R);

    Mat color;

    merge(array_to_merge, color);

    imwrite(name, color);
}

void writetofile2(vec3<float> &colorimages, const char *name)
{
    /*
        Another function to write color images to the file, but use this if they are of shape 3XmXn, instead of mXnX3
        We encounter this shape in the function debayer_rectify
    */

    vector<Mat> array_to_merge;

    Mat R(colorimages[0].size(), colorimages[0].at(0).size(), CV_32FC1);
    for (int i = 0; i < R.rows; ++i)
    {
        for (int j = 0; j < R.cols; ++j)
        {
            R.at<float>(i, j) = colorimages.at(0).at(i).at(j);
        }
    }
    Mat G(colorimages[0].size(), colorimages[0].at(0).size(), CV_32FC1);
    for (int i = 0; i < G.rows; ++i)
    {
        for (int j = 0; j < G.cols; ++j)
        {
            G.at<float>(i, j) = colorimages.at(1).at(i).at(j);
        }
    }

    Mat B(colorimages[0].size(), colorimages[0].at(0).size(), CV_32FC1);
    for (int i = 0; i < B.rows; ++i)
    {
        for (int j = 0; j < B.cols; ++j)
        {
            B.at<float>(i, j) = colorimages.at(2).at(i).at(j);
        }
    }

    array_to_merge.push_back(B);
    array_to_merge.push_back(G);
    array_to_merge.push_back(R);

    Mat color;

    merge(array_to_merge, color);

    imwrite(name, color);
}

void process_params(std::string &param_file, std::string &compressed_LUTx_path, std::string &compressed_LUTy_path, int &decay_f_power, 
                    int &n_nearest_neighbours, string &distance_metric, int &decay_func_exp, bool &are_LUTs_compressed, int &max_buffer_width){
    std::ifstream i(param_file.c_str());
    json j;
    i >> j;
    if(j.contains("LUTx_path")){
        compressed_LUTx_path = j["LUTx_path"];
    } else{
        cout<<"param file missing required variable LUTx_path. Exiting\n";
        exit(1);
    }
    if(j.contains("LUTy_path")){
        compressed_LUTy_path = j["LUTy_path"];
    } else{
        cout<<"param file missing required variable LUTy_path. Exiting\n";
        exit(1);
    }

    if(j.contains("decay_f_power")){
        decay_f_power = j["decay_f_power"];
    } else{
        decay_f_power = 2;
    }

    if(j.contains("n_nearest_neighbours")){
        n_nearest_neighbours = j["n_nearest_neighbours"];
    } else{
        n_nearest_neighbours = 15;
    }
    
    if(j.contains("distance_metric")){
        distance_metric = j["distance_metric"];
    } else{
        distance_metric = "l2";
    }

    if(j.contains("decay_func_exp")){
        decay_func_exp = j["decay_func_exp"];
    } else{
        decay_func_exp = 1;
    }
    
    if(j.contains("are_LUTs_compressed")){
        are_LUTs_compressed = j["are_LUTs_compressed"];
    } else{
        are_LUTs_compressed = true;
    }
    
    if(j.contains("max_buffer_width")){
        max_buffer_width = j["max_buffer_width"];
    } else{
        max_buffer_width = 100;
    }
}
void process_args(int argc, char *argv[], std::string &input_image_path, std::string &output_path, std::string &compressed_LUTx_path, std::string &compressed_LUTy_path,
                    int &decay_f_power, int &n_nearest_neighbours, std::string &distance_metric, int &decay_func_exp, bool &are_LUTs_compressed, int &max_buffer_width){
    int opt; 
    std::string params_file;
    while ((opt = getopt(argc,argv,"I:O:P:h?")) != EOF)
    {  
        switch(opt)  
        {  
            case 'I':{
                input_image_path = optarg;
                continue;
            }
            case 'O':{
                output_path = optarg;
                continue;
            }
            case 'P':{
                params_file = optarg;
                process_params(params_file, compressed_LUTx_path, compressed_LUTy_path, decay_f_power, 
                    n_nearest_neighbours, distance_metric, decay_func_exp, are_LUTs_compressed, max_buffer_width);
                continue;
            }
            case '?':
            case 'h':
            default:{
                cout << "Usage: ./test -I <input_image_path> -O <output_path> -P <param file>\n";
                break;
            }
            case -1:{
                break;
            }
        }
    }
}