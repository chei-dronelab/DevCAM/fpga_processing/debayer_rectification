#include<tuple>
#include<vector>
#ifndef LUT_H
#define LUT_H

class LUT{
    float xrmin,xrmax,yrmin,yrmax;
    std::vector<std::vector<float>> LUT_fwdf_x,LUT_fwdf_y, compressed_LUT_yout_fwd, compressed_LUT_xout_fwd;
    int poly_degree;
    bool are_LUTs_compressed;
    std::tuple<float,float,float> to_homog(std::tuple<float,float> points);
    std::tuple<float,float> from_homog(std::tuple<float,float,float> points);
    void compress_LUT_fwd_only(int poly_degree);

public:
    LUT();
    LUT(std::vector<std::vector<float>> proj_xform_mat, std::tuple<int,int> img_size);
    LUT(bool are_LUTs_compressed, std::vector<std::vector<float>> LookUpTablex, std::vector<std::vector<float>> LookUpTabley);
    std::tuple<float,float> LUT_fwdf2point(int i_in,int j_in);
    std::tuple<int, int> get_output_shape_xy();
};

#endif