#include <vector>
#include <string>
#include <stdio.h> 

#ifndef FUNCTIONS_H
#define FUNCTIONS_H
enum BAYER
{
    BAYER_BGGR,
    BAYER_GRBG,
    BAYER_GBRG,
    BAYER_RGGB
};

template <typename T>
using vec3 = std::vector <std::vector <std::vector <T>>>;

template <typename T>
using vec2 = std::vector <std::vector <T>>;


void norm(vec2<float> &image);

float distance(float x1, float y1, float x2, float y2, std::string metric);

std::vector<std::tuple<float, int, int>> n_nearest_neighbours(float x, float y, int n, std::string metric);

void debayer_rectify(BAYER bayer_pattern, vec2<float> source_image_bayer,
                     class LUT &tables, vec3<float> &output_stream, 
                     std::tuple<int, int> output_image_size,
                     vec3<float> &vis_buffer, 
                     vec3<float> &vis_sec_buffer, 
                     int decay_f_power, int nn,
                     std::string nn_dist_metric, int decay_func_exp, int max_buffer_width);

vec2<float> fileToVector(const char *name);

void writetofile(vec2<float> &gray, const char *name);
void writetofile(vec3<float> &colorimages, const char *name);

void writetofile2(vec3<float> &colorimages, const char *name);

void process_args(int argc, char *argv[], std::string &input_image_path, std::string &output_path, std::string &compressed_LUTx_path, std::string &compressed_LUTy_path,
                    int &decay_f_power, int &n_nearest_neighbours, std::string &distance_metric, int &decay_func_exp, bool &are_LUTs_compressed, int &max_buffer_width);

void process_params(std::string &param_file, std::string &compressed_LUTx_path, std::string &compressed_LUTy_path, int &decay_f_power, 
                    int &n_nearest_neighbours, std::string &distance_metric, int &decay_func_exp, bool &are_LUTs_compressed, int &max_buffer_width);
#endif