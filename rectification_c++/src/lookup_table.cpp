#include <climits>
#include <cmath>
#include <vector>
#include <tuple>
#include <assert.h>
#include <float.h>
#include <iostream>
#include "lookup_table.hpp"

#define max(a, b) a > b ? a : b
#define min(a, b) a < b ? a : b

using namespace std;

tuple<float, float, float> LUT::to_homog(tuple<float, float> points)
{
    return tuple<float, float, float>(get<0>(points), get<1>(points), 1.0);
}

tuple<float, float> LUT::from_homog(tuple<float, float, float> points)
{
    return tuple<float, float>(get<0>(points) / get<2>(points), get<1>(points) / get<2>(points));
}
LUT::LUT()
{
    cout << "Default Constructor in the LUT class. Hi!\n";
}

LUT::LUT(vector<vector<float>> proj_xform_mat, tuple<int, int> img_size)
{
    /*
        Constructor for the LUT class to initialise a Look Up Table from projective Transformation matrix H using the following expression between input and output points:
        [x_out,y_out,z_out]^T = matrix_multiply(H, [x,y,1]^T)
        
        LUT_x[y,x] = x_out / z_out
        LUT_y[y,x] = y_out / z_out
        
        Then compress that look up table.

        */
    assert(tuple_size<decltype(img_size)>::value >= 2);
    assert(proj_xform_mat.size() >= 3 && proj_xform_mat[0].size() >= 3);

    this->LUT_fwdf_x = vector<vector<float>>(get<0>(img_size), vector<float>(get<1>(img_size), 0));
    this->LUT_fwdf_y = vector<vector<float>>(get<0>(img_size), vector<float>(get<1>(img_size), 0));

    this->xrmin = FLT_MAX;
    this->xrmax = FLT_MIN;
    this->yrmin = FLT_MAX;
    this->yrmax = FLT_MIN;
    for (int xin = 0; xin < get<1>(img_size); xin++)
    {
        for (int yin = 0; yin < get<0>(img_size); yin++)
        {
            tuple<float, float, float> coord(float(xin), float(yin), 1.0);
            float x, y;
            x = (proj_xform_mat[0][0] * xin + proj_xform_mat[0][1] * yin + proj_xform_mat[0][2]) / (proj_xform_mat[2][0] * xin + proj_xform_mat[2][1] * yin + proj_xform_mat[2][2]);
            y = (proj_xform_mat[1][0] * xin + proj_xform_mat[1][1] * yin + proj_xform_mat[1][2]) / (proj_xform_mat[2][0] * xin + proj_xform_mat[2][1] * yin + proj_xform_mat[2][2]);

            this->LUT_fwdf_x[yin][xin] = x;
            this->LUT_fwdf_y[yin][xin] = y;
            this->xrmin = min(this->xrmin, x);
            this->xrmax = max(this->xrmax, x);
            this->yrmin = min(this->yrmin, y);
            this->yrmax = max(this->yrmax, y);
        }
    }

    this->poly_degree = 3;
    this->are_LUTs_compressed = false;
    this->compress_LUT_fwd_only(poly_degree = this->poly_degree);
}

LUT::LUT(bool are_LUTs_compressed, vector<vector<float>> LookUpTablex, vector<vector<float>> LookUpTabley)
{
    /*
        Constructor for the LUT class, uses user provided LUTs and compresses them. 
        */
    this->are_LUTs_compressed = are_LUTs_compressed;
    this->LUT_fwdf_x = LookUpTablex;
    this->LUT_fwdf_y = LookUpTabley;

    this->poly_degree = 3;
    this->compress_LUT_fwd_only(this->poly_degree);
}

/*
LookUpTablex[yin,xin]= xout
LookUpTabley[yin,xin]= yout

Original LUT was of size maxy X maxx
We use polynomial approximation to compress this table.
Compress_along_axis = 0
Yf=Pn(Yi) (a polynomial P of order n)
Pn = F(Xi) (coefficients of the polynomial Pn are a function F of Xi)
Where Pn is a polynomial of degree n
LUT will become a table of size  maxx X (n+1)
Compress_along_axis = 1
Xf=Pn(Xi)
Pn = F(Yi)
Where Pn is a polynomial of degree n
LUT will become a table of size maxy X (n+1)
*/

void LUT::compress_LUT_fwd_only(int poly_degree = 3)
{
    /*
        Empty function because we are directly feeding compressed LUTs
        */
    return;
}

tuple<float, float> LUT::LUT_fwdf2point(int i_in, int j_in)
{
    /*
        Main wrapper function. Feed in the input pixel coordinates, 
        it reads the polynomial coefficients stored in the LUT,
        and computes the output pixel coordinates.
        */
    float xval, yval;
    if (!this->are_LUTs_compressed)
    {
        xval = this->LUT_fwdf_x[i_in][j_in];
        yval = this->LUT_fwdf_y[i_in][j_in];
    }
    else
    {
        vector<float> xcoeffs = this->LUT_fwdf_x[i_in];
        vector<float> ycoeffs = this->LUT_fwdf_y[i_in];

        float xvar = 1.0, yvar = 1.0;
        xval = 0.0;
        yval = 0.0;
        for (int x = xcoeffs.size() - 1; x >= 0; x--)
        {
            xval += xcoeffs[x] * xvar;
            yval += ycoeffs[x] * xvar;
            xvar *= j_in;
            yvar *= i_in;
        }
    }

    return make_tuple(xval, yval);
}

tuple<int, int> LUT::get_output_shape_xy()
{
    /*
        Assumes that the LUT is properly adjusted.
        That is, xrmin and yrmin should be 0,
        */
    float xrmin = 100000, xrmax = -100000, yrmin = 100000, yrmax = -100000;
    tuple<float, float> pt;
    for (int j = 0; j < this->LUT_fwdf_x.size(); j++)
    {
        for (int i = 0; i < this->LUT_fwdf_y.size(); i++)
        {
            pt = LUT_fwdf2point(i, j);

            xrmin = min(xrmin, get<0>(pt));
            xrmax = max(xrmax, get<0>(pt));
            yrmin = min(yrmin, get<1>(pt));
            yrmax = max(yrmax, get<1>(pt));
        }
    }
    int xrange = int(ceil(xrmax - xrmin));
    int yrange = int(ceil(yrmax - yrmin));
    cout << "Output shape xy: " << xrange << ", " << yrange << "\n";
    return make_tuple(xrange, yrange);
}
