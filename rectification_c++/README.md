To run the program, go to the src folder and run the following command:

`./build.sh`

Then, run the binary using the following command:

`./output -I <path to input image> -O <path to output image> -P <Path to parameters file>`
