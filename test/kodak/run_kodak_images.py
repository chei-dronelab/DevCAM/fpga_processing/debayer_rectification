import os
from pathlib import Path
import subprocess
from generate_kodak_LUTs import generate_identity_LUTs

input_path = os.path.abspath("./kodak_input_images/")
output_path = os.path.abspath("./kodak_output_cpp/")

kodak_data_folder = os.path.abspath("./data")
utils_dir = os.path.abspath("../../utils/")

# Since Kodak images need no rectification, 
# we perform debayering+rectification using identity LUTs
# and we save some time by generating compressed LUTs directly
generate_identity_LUTs()

print("Generated LUTs")

wd = os.path.abspath("../../rectification_c++/src/")
length = len(os.listdir(input_path))
print("Processing all images")
for idx, files in enumerate(os.listdir(input_path)):
    print("{}/{}\r".format(idx, length), end = '')
    input_image_path = os.path.join(input_path, files)
    output_image_path = os.path.join(output_path, files)
    out = subprocess.run(["./output", "-I", input_image_path, "-O", output_image_path,
                    "-P", os.path.join(kodak_data_folder, "kodak_params.json")], cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print("Done processing")