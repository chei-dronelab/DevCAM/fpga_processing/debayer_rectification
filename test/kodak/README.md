Kodak dataset from http://r0k.us/graphics/kodak/, in kodak_input_images folder.
Since Kodak images are already rectified, we perform debayering + rectification with identity LUTs.
Our code converts RGB data to bayered image, which are then processed, and stored in kodak_output_cpp. 
For comparison, debayering output from OpenCV is in kodak_output_opencv.

To run debayering + rectification on kodak dataset: 
`python3 run_kodak_images.py`
