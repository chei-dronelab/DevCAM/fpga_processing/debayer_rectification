import json
import numpy as np
import cv2
import os

def generate_identity_LUTs():
    # Since Kodak data involves no rectification, we generate a LUT with identity mapping
    kodak_img_shape = cv2.imread('./kodak_input_images/kodim01.png').shape[:2]
    lutx = np.zeros((
        min(list(kodak_img_shape)), 3
    ), np.float32)
    luty = np.zeros_like(lutx, np.float32)
    lutx[:,-2] = 1.0
    luty[:,-1] = np.arange(0, luty.shape[0])

    fn1 = os.path.abspath('./data/compressed_lutx.txt')
    fn2 = os.path.abspath('./data/compressed_luty.txt')

    with open(fn1,'w') as f1:
        for i in range(lutx.shape[0]):
            s = " ".join([str(j) for j in lutx[i]]) + "\n"
            f1.write(s)
        f1.close()
    with open(fn2,'w') as f1:
        for i in range(luty.shape[0]):
            s = " ".join([str(j) for j in luty[i]]) + "\n"
            f1.write(s)
        f1.close()

    # And the corresponding parameters json file  
    d = {
        "LUTx_path": fn1,
        "LUTy_path": fn2,
        "decay_f_power": 2,
        "n_nearest_neighbours": 15,
        "distance_metric": "l2",
        "decay_func_exp": 2,
        "are_LUTs_compressed": True,
        "max_buffer_width":100
    }
    with open('./data/kodak_params.json', 'w') as f1:
        json.dump(d, f1)

if __name__ == '__main__':
    generate_identity_LUTs()