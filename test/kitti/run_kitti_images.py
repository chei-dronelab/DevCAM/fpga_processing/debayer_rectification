import os
from pathlib import Path
import subprocess
from generate_opencv_LUT_from_datafile import generate_opencv_LUTs

left_camera_input_path = os.path.abspath("./input_images/image_02/data/")
right_camera_input_path = os.path.abspath("./input_images/image_03/data/")

left_camera_output_path = os.path.abspath("./output_images/image_02/")
right_camera_output_path = os.path.abspath("./output_images/image_03/")

kitti_data_folder = os.path.abspath("./data")
utils_dir = os.path.abspath("../../utils/")

# Using the stereo parameters described for the KITTI datasets, generate OpenCV's LUTs for left and right cameras


# silly get around to allow passing dict as args from argparse
class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

args = {
    'calibration_file': os.path.join(kitti_data_folder, 'calib_cam_to_cam.txt'),
    'lutx_path': os.path.join(kitti_data_folder, 'kitti_opencv_lutx.txt'),
    'luty_path': os.path.join(kitti_data_folder, 'kitti_opencv_luty.txt'),
}

args = dotdict(args)
sz0, sz1 = generate_opencv_LUTs(args)

print("Generated OpenCV based LUTs")

# Invert and Compress OpenCV's LUTs for left and right cameras
subprocess.run(['python3', os.path.join(utils_dir, 'process_LUTs.py'),
                "--lutx", os.path.join(kitti_data_folder, 'kitti_opencv_lutx_left.txt'),
                "--luty", os.path.join(kitti_data_folder, 'kitti_opencv_luty_left.txt'),
                "--height", "512",
                "--width", "1392",
                "--param_file", os.path.join(kitti_data_folder, 'kitti_params_left_camera.json'),
                "--decay_f_power", " 2",
                "--n_nearest_neighbours", "15",
                "--distance_metric", "l2",
                "--decay_func_exp", "2",
                "--poly_degree", "8",
                ], stdout=subprocess.PIPE)

print("Compressed LUTs for left camera")

subprocess.run(['python3', os.path.join(utils_dir, 'process_LUTs.py'),
                "--lutx", os.path.join(kitti_data_folder, 'kitti_opencv_lutx_right.txt'),
                "--luty", os.path.join(kitti_data_folder, 'kitti_opencv_luty_right.txt'),
                "--height", "512",
                "--width", "1392",
                "--param_file", os.path.join(kitti_data_folder, 'kitti_params_right_camera.json'),
                "--decay_f_power", " 2",
                "--n_nearest_neighbours", "15",
                "--distance_metric", "l2",
                "--decay_func_exp", "2",
                "--poly_degree", "8",
                ], stdout=subprocess.PIPE)

print("Compressed LUTs for right camera")
wd = os.path.abspath("../../rectification_c++/src/")
length = len(os.listdir(left_camera_input_path))

print("Processing all images for left camera")
for idx, files in enumerate(os.listdir(left_camera_input_path)):
    print("{}/{}\r".format(idx, length), end = '')
    input_image_path = os.path.join(left_camera_input_path, files)
    output_image_path = os.path.join(left_camera_output_path, files)
    subprocess.run(["./output", "-I", input_image_path, "-O", output_image_path,
                    "-P", os.path.join(kitti_data_folder, "kitti_params_left_camera.json")], cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

print("Processing all images for right camera")
length = len(os.listdir(right_camera_input_path))
for idx, files in enumerate(os.listdir(right_camera_input_path)):
    print("{}/{}\r".format(idx, length), end = '')
    input_image_path = os.path.join(right_camera_input_path, files)
    output_image_path = os.path.join(right_camera_output_path, files)
    subprocess.run(["./output", "-I", input_image_path, "-O", output_image_path,
                    "-P", os.path.join(kitti_data_folder, "kitti_params_right_camera.json")], cwd=wd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
