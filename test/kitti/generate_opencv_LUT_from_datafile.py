import cv2
import numpy as np
import os
from numpy import polyfit
import argparse

def generate_opencv_LUTs(args):
    cd = ''
    with open(args.calibration_file) as f1:
        cd = f1.read().strip().split('\n')

    calib_data = {}
    for k in cd:
        if 'calib_time' in k:
            continue
        d = k.strip().split(':')
        calib_data[d[0]] = [float(d2) for d2 in d[1].strip().split(' ')]


    K0 = np.asarray(calib_data['K_02']).reshape((3,3))
    K1 = np.asarray(calib_data['K_03']).reshape((3,3))
    R0 = np.asarray(calib_data['R_rect_02']).reshape((3,3))
    R1 = np.asarray(calib_data['R_rect_03']).reshape((3,3))
    D0 = np.asarray(calib_data['D_02'])
    D1 = np.asarray(calib_data['D_03'])
    P0 = np.asarray(calib_data['P_rect_02']).reshape((3,4))
    P1 = np.asarray(calib_data['P_rect_03']).reshape((3,4))
    sz0 = tuple([int(i) for i in calib_data['S_02']])
    sz1 = tuple([int(i) for i in calib_data['S_03']])

    m0x, m0y = cv2.initUndistortRectifyMap(K0,D0,R0,P0,sz0, cv2.CV_32F)
    m1x, m1y = cv2.initUndistortRectifyMap(K1,D1,R1,P1,sz1, cv2.CV_32F)

    lutx_fn, lutx_ext = os.path.splitext(args.lutx_path)
    luty_fn, luty_ext = os.path.splitext(args.luty_path)

    files = [lutx_fn+'_left'+lutx_ext, lutx_fn+'_right'+lutx_ext, luty_fn+'_left'+luty_ext, luty_fn+'_right'+luty_ext]
    maps = [m0x, m1x, m0y, m1y]
    for f,m in zip(files,maps):
        if f.endswith('.npy'):
            np.save(f,m)
        else:
            with open(f,'w') as f1:
                for i in range(m.shape[0]):
                    s = " ".join([str(j) for j in m[i]]) + "\n"
                    f1.write(s)
            f1.close()
    return sz0, sz1

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--calibration_file', required=True, help = 'Path to calibration file')
    parser.add_argument('--lutx_path', required=True, help = 'Output path for Opencv LUT for x coordinates')
    parser.add_argument('--luty_path', required=True, help = 'Output path for Opencv LUT for y coordinates')
    args = parser.parse_args()
    generate_opencv_LUTs(args)
