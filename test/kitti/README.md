The folder input_images contains raw KITTI data from RGB cameras (cameras 2 and 3). We store the LookUp Tables and parameter json files for both cameras in data/ folder.
The output from our debayering + rectification algorithm will be stored in output_images folder.
To run the code, simply run:

`python3 run_kitti_images.py`


