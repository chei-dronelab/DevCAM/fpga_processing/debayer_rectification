import cv2
import numpy as np
from colormath.color_diff import delta_e_cie1976, delta_e_cie1994, delta_e_cie2000, delta_e_cmc
from colormath.color_objects import LabColor

def test_psnr(ref_image_path='ref_image.png', output_image_path = 'test_output_stream.png'):
#     ref_image = cv2.convertScaleAbs(cv2.imread(ref_image_path),alpha=(255.0))
#     output_image = cv2.convertScaleAbs(cv2.imread(output_image_path),alpha = (255.0))
    ref_image = cv2.imread(ref_image_path)
    output_image = cv2.imread(output_image_path)
    assert ref_image.shape == output_image.shape
    assert ref_image.max() == output_image.max()
    diff = np.subtract(ref_image,output_image)
    mse = np.sum(np.multiply(diff,diff)) / np.product(diff.shape)

    psnr = 20.0 * np.log10(ref_image.max()) - 10*np.log10(mse)

    return psnr

def crop_image(input_image_path,output_image_path):
	im = cv2.imread(input_image_path)
	img_gray = cv2.convertScaleAbs(cv2.cvtColor(im, cv2.COLOR_RGB2GRAY),alpha=(255.0)).astype(np.uint8)
	_,thresh = cv2.threshold(img_gray,1,255,cv2.THRESH_BINARY)
	contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnt = contours[0]
	x,y,w,h = cv2.boundingRect(cnt)
	crop = cv2.convertScaleAbs(im[y:y+h,x:x+w])
	cv2.imwrite(output_image_path, crop)
	print("Output image size: ",crop.shape)

def eab_error(ref_image_path='ref_image.png', output_image_path = 'test_output_stream.png', measure = 'default'):
	ref_image = cv2.convertScaleAbs(cv2.imread(ref_image_path),alpha=(255.0))
	output_image = cv2.convertScaleAbs(cv2.imread(output_image_path),alpha = (255.0))
	assert ref_image.shape == output_image.shape

	Lab_ref = cv2.cvtColor(ref_image, cv2.CV_RGB2Lab)
	Lab_out = cv2.cvtColor(output_image, cv2.CV_RGB2Lab)

	if lower(string(measure)) == 'default':
		f = delta_e_cie1976
	elif lower(string(measure)) == '1994':
		f = delta_e_cie1994
	elif lower(string(measure)) == '2000':
		f = delta_e_cie2000
	elif lower(string(measure)) == 'cmc':
		f = delta_e_cmc
	else:
		f = delta_e_cie1976

	total_score = 0.0
	for y in range(ref_image.shape[0]):
		for x in range(ref_image.shape[1]):
			total_score += f(LabColor(Lab_ref[y,x]),LabColor(Lab_out[y,x]))
	total_score/= np.product(ref_image.shape[:2])

	return total_score


	# 	err_l = np.subtract(Lab_ref[:,:,0],Lab_out[:,:,0])
	# 	err_a = np.subtract(Lab_ref[:,:,1],Lab_out[:,:,1])
	# 	err_b = np.subtract(Lab_ref[:,:,2],Lab_out[:,:,2])

	# 	err_l2 = np.multiply(err_l,err_l)
	# 	err_a2 = np.multiply(err_a,err_a)
	# 	err_b2 = np.multiply(err_b,err_b)

	# 	err_sq = np.add(np.add(err_l2,err_a2),err_b2)

	# 	return np.sum(np.sqrt(err_sq))

	# if lower(string(measure)) == '1994':

	# 	delta_l = np.subtract(Lab_ref[:,:,0]-Lab_out[:,:,0])

	# 	c1 = np.sqrt(np.add(np.multiply(Lab_ref[:,:,1],Lab_ref[:,:,1]), np.multiply(Lab_ref[:,:,2],Lab_ref[:,:,2])))

	# 	c2 = np.sqrt(np.add(np.multiply(Lab_out[:,:,1],Lab_out[:,:,1]), np.multiply(Lab_out[:,:,2],Lab_out[:,:,2])))

	# 	delta_c = np.subtract(c1,c2)
	# 	delta_c2 = np.multiply(delta_c, delta_c)

	# 	err_a = np.subtract(Lab_ref[:,:,1],Lab_out[:,:,1])
	# 	err_b = np.subtract(Lab_ref[:,:,2],Lab_out[:,:,2])

	# 	err_a2 = np.multiply(err_a,err_a)
	# 	err_b2 = np.multiply(err_b,err_b)
	# 	delta_h = np.sqrt( np.subtract( np.add(err_a2, err_b2) , delta_c2))

	# 	k = [1,0.045,0.015]

	# 	Sc_2 = np.multiply(np.add(1,k[1]*c1),np.add(1,k[1]*c1))
	# 	Sh_2 = np.multiply(np.add(1,k[2]*c1),np.add(1,k[2]*c1))

	# 	e94_sq = np.multiply(delta_l,delta_l) + np.divide(delta_c2,Sc_2) + np.divide(np.multiply(delta_h,delta_h),Sh_2)

	# 	return np.sum(np.sqrt(e94_sq))

