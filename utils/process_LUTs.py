import numpy as np
import os
from numpy import polyfit
from sklearn.neighbors import KDTree
import argparse
import json

def invert_mapping(iL_shape,oL_shape,mapx,mapy):
    
    points=np.asarray(list(zip(mapx.reshape(-1, 1),mapy.reshape(-1, 1)))).reshape(-1,2)
    print(points.shape)
    tree=KDTree(points)
    k=15
    table_shape=iL_shape[:2]
    invmapx=np.zeros((table_shape[1],table_shape[0]))
    invmapy=np.zeros((table_shape[1],table_shape[0]))
    print(invmapx.shape)
    for yd in range(iL_shape[0]):
        for xd in range(iL_shape[1]):

            dists,idxs=tree.query(X=np.asarray([xd,yd]).reshape((1,2)),k=k,dualtree=True)
            idxs=np.asarray(idxs).ravel()
            dists=np.asarray(dists).ravel()
            if dists[0]==0.0:
                invmapx[xd,yd]=int(idxs[0]%oL_shape[1])
                invmapy[xd,yd]=idxs[0]/oL_shape[1]
            else:
                xnumr8r=np.sum([(idxs[j]%oL_shape[1])/(dists[j]**2) for j in range(k)])
                ynumr8r=np.sum([(idxs[j]/oL_shape[1])/(dists[j]**2) for j in range(k)])
                denom=np.sum([(1.0/dists[j]**2) for j in range(k)])
                invmapx[xd,yd]=xnumr8r/denom
                invmapy[xd,yd]=ynumr8r/denom
    return invmapx,invmapy

def compress_LUT_fwd_only_same_size(LUT_fwd_x,LUT_fwd_y,poly_degree=4):
    '''Original LUT was of size maxy X maxx
    We use polynomial approximation to compress this table.
    Compress_along_axis = 0
    Yf=Pn(Yi)
    Pn = F(Xi)
    Where Pn is a polynomial of degree n
    LUT will become a table of size  maxx X (n+1)
    Compress_along_axis = 1
    Xf=Pn(Xi)
    Pn = F(Yi)
    Where Pn is a polynomial of degree n
    LUT will become a table of size maxy X (n+1)
    '''
    maxy,maxx=LUT_fwd_x.shape[:2]
    compressed_LUT_yout_fwd=np.zeros((maxy,poly_degree+1))
    compressed_LUT_xout_fwd=np.zeros((maxy,poly_degree+1))
    for idx in range(maxy):
        yu = LUT_fwd_y[idx,:]
        # _,yu=self.LUT_fwdf(idx,None)
        x=range(maxx)
        values, _, _, _, _=polyfit(x,yu,deg=poly_degree,full=True)
        compressed_LUT_yout_fwd[idx,:]=values
        
    for idx in range(maxy):
        xu = LUT_fwd_x[idx,:]
        # xu,_=self.LUT_fwdf(None,idx)
        y=range(maxx)
        values,_, _, _, _=polyfit(y,xu,deg=poly_degree,full=True)
        compressed_LUT_xout_fwd[idx,:]=values
    return compressed_LUT_xout_fwd, compressed_LUT_yout_fwd


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-x", "--lutx", required = True, help = "Initial LUT for x coordinates")
    parser.add_argument("-y", "--luty", required = True, help = "Initial LUT for y coordinates")
    parser.add_argument("--fwd", action='store_true', help = "Use this flag if the LUTs are forward mapping based, not OpenCV style bwd mapping based")
    parser.add_argument("-H","--height", required = True,  type = int, help = "Image height in pixels")
    parser.add_argument("-W","--width", required = True, type = int, help = "Image width in pixels")
    parser.add_argument("--out_height", default = None,  type = int, help = "height of output image, in pixels (assumed to be same as input height, if unspecified)")
    parser.add_argument("--out_width", default = None,  type = int, help = "width of output image, in pixels (assumed to be same as input width, if unspecified)")    
    parser.add_argument("-p","--param_file", required = True, help = "Path to output params file")
    parser.add_argument("--decay_f_power", default =  2, type = int, help = "Power to which the decay function in splatting is raised (f^power). Default = 2")
    parser.add_argument("--n_nearest_neighbours", default =  15, type = int, help = "Number of neighbours to splat to")
    parser.add_argument("--distance_metric", default =  "l2", help = "Distance metric to use (l1/ l2). Default = l2")
    parser.add_argument("--decay_func_exp", default =  2, type = int, help = "Decay function is exp(-distance^(decay_f_power)) if set to 1, else 1/1+distance^(decay_f_power) if set to 2")
    parser.add_argument("--nocompress", action= 'store_true', help = "Use LUTs without compression?")
    parser.add_argument("-d", "--poly_degree", default = 8, type=int, help = "Degree of polynomial used for polynomial fitting based LUT compression. Default = 8")
    args = parser.parse_args()

    lutx = None
    luty = None

    lutx_fn, _ = os.path.splitext(args.lutx)
    luty_fn, _ = os.path.splitext(args.luty)
    
    
    if args.lutx[-4:] != '.npy':
        with open(args.lutx) as fx:
            data = fx.read().strip().split('\n')
            print("lines: ",len(data))
            print(len(data[0].strip().split()))
            lutx = np.asarray([[float(d) for d in l.split()] for l in data]).astype(np.float32)
    else:
        lutx = np.load(args.lutx)

    if args.luty[-4:] != '.npy':
        with open(args.luty) as fy:
            luty = np.asarray([[float(d) for d in l.strip().split()] for l in fy.read().strip().split('\n')]).astype(np.float32)
    else:
        luty = np.load(args.luty)

    assert lutx.shape == (args.height, args.width), print("Shape of LUTx ({}) does not match image shape ({},{})".format(lutx.shape, args.height, args.width))
    assert luty.shape == (args.height, args.width), print("Shape of LUTy ({}) does not match image shape ({},{})".format(luty.shape, args.height, args.width))

    iL_shape = (args.height, args.width)
    oL_shape = (args.out_height if args.out_height is not None else args.height, args.out_width if args.out_width is not None else args.width)

    if not args.fwd:
        print("Inverting LUTs")
        inv_mapx, inv_mapy = invert_mapping(iL_shape, oL_shape, lutx, luty)
        print("LUTs inverted, compressing them now.")
    else:
        inv_mapx, inv_mapy = lutx.T, luty.T
    
    cmpx, cmpy = compress_LUT_fwd_only_same_size(inv_mapx.T, inv_mapy.T, args.poly_degree)
    print("Compression done.")
    files = [lutx_fn+'_compressed', luty_fn+'_compressed']
    maps = [cmpx, cmpy]
    for f,m in zip(files,maps):
        with open(f+'.txt','w') as f1:
            for i in range(m.shape[0]):
                s = " ".join([str(j) for j in m[i]]) + "\n"
                f1.write(s)
        f1.close()
    
    params_dict = {}
    params_dict['decay_f_power'] = int(args.decay_f_power)
    params_dict['decay_func_exp'] = int(args.decay_func_exp)
    params_dict['distance_metric'] = args.distance_metric
    params_dict['n_nearest_neighbours'] = int(args.n_nearest_neighbours)
    params_dict["are_LUTs_compressed"] = True
    params_dict["LUTx_path"] = os.path.abspath(lutx_fn+'_compressed.txt')
    params_dict["LUTy_path"] = os.path.abspath(luty_fn+'_compressed.txt')
    with open(args.param_file,'w') as f1:
        json.dump(params_dict, f1)
    

if __name__ == '__main__':
    main()