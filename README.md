# Splatty- A Unified Image Demosaicing and Rectification Method


Image demosaicing and rectification are key tasks that are frequently used in many computer vision systems. To date, however, their implementations have been plagued with large memory requirements and inconvenient dataflow, making it difficult to scale them to real-time, high resolution settings.
This has motivated the development of joint demosaicing and rectification algorithms that resolve the backward mapping dataflow for improved hardware implementation. Towards this purpose, we propose Splatty: an algorithmic solution to pipelined image stream demosaicing and rectification for memory bound applications requiring computational efficiency.

We begin by introducing a polynomial Look-up-Table (LUT) compression scheme that can encode any arbitrarily complex lens model for rectification while keeping the remapping errors below 1E-10 pixels, and reducing the memory footprint to O(min(m,n)) from O(mn) for an n x n sized image. The core contribution leverages this LUT for a unified, forward-only splatting algorithm for simultaneous demosaicing and rectification. We demonstrate that merging these two steps into a single, forward-only splatting pass with interpolation, provides distinctive dataflow and performance efficiency benefits while maintaining quality standards when compared to state-of-the-art demosaicing and rectification algorithms.

This work has been published and is accesible under:

WACV 2021, "Splatty- A Unified Image Demosaicing and Rectification Method", P. Verma, D. E. Meyer, H. Xang, F. Kuester.

### Building and running code

- To build this code, go to rectification_c++/src/ and call the `build_code.sh` script

- For running this code, you need to create a json file with the parameters required for running. You can do so using the `process_LUTs.py` script in `utils` folder, which can be called in this way:

`python3 utils/process_LUTs.py -x <path to OpenCV LUT for x coordinates> -y <path to OpenCV LUT for y coordinates> -H <image height> -W <image width> -p <output parameter.json file>`

- Additional options can be passed to `process_LUTs.py` to tweak the splatting function's functionality, and can be accessed using 
`python3 utils/process_LUTs.py -h`

- Finally, splatty can be run as follows:

`cd rectification_c++/src/`

`./output -I </path/to/input/image> -O <path/to/output/image> -P </path/to/parameter.json>`

- Sample scripts for running on [KITTI data](http://www.cvlibs.net/datasets/kitti/raw_data.php) and [Kodak dataset](http://r0k.us/graphics/kodak/) are present in `test/kitti` and `test/kodak` respectively.

To run them, simply call the python scripts in the folders:

`python3 test/kitti/run_kitti_images.py`

`python3 test/kodak/run_kodak_images.py`


### Dependencies
- The C++ code uses the json library by Niels Lohmann ([link](https://github.com/nlohmann/json/)), and OpenCV
- It also uses pkg-config to provide the include paths for the linker
- The Python code requires Python 3.5+, and uses the following libraries:
    - OpenCV
    - Numpy
    - Scikit-learn
