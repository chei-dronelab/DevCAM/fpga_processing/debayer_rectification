#include "Splatty_top.hpp"

void Splatty_top(hls::stream<uint_8_side_channel> &inStream, hls::stream<coeff_side_channel> &xcoeffStream, hls::stream<coeff_side_channel> &ycoeffStream,
		hls::stream<uint_64_side_channel> &outStream, BAYER bayer_pattern) {
#pragma HLS INTERFACE axis port=inStream
#pragma HLS INTERFACE axis port=outStream
#pragma HLS INTERFACE s_axilite port=bayer_pattern
#pragma HLS INTERFACE s_axilite port=return bundle=CRTL_BUS

	static INTENSITY row_buffer[IMG_WIDTH_OR_COLS];
	static int how_many_pixels_landed_here[MAX_VERTICAL_REMAP_DIFF];
	static float xcoeffs[POLY_DEGREE];
	static float ycoeffs[POLY_DEGREE];
#pragma HLS array_partition variable=how_many_pixels_landed_here complete
#pragma HLS array_partition variable=xcoeffs complete
#pragma HLS array_partition variable=ycoeffs complete

	static half output_buffer[3][MAX_VERTICAL_REMAP_DIFF][IMG_WIDTH_OR_COLS];
	static half secondary_buffer2[3][MAX_VERTICAL_REMAP_DIFF][IMG_WIDTH_OR_COLS];

	static int op_buffer_width = MAX_VERTICAL_REMAP_DIFF;
	static int decay_func_exp = 1;
	static int decay_f_power = 2;
	static int shape_1 = 3;
	static int shape_2 = op_buffer_width;
	static int shape_3 = IMG_WIDTH_OR_COLS;
	static int row = IMG_HEIGHT_OR_ROWS;
	static int col = IMG_WIDTH_OR_COLS;
	static int compensate_middle = 60;

	int miny=0;
	int row_count = -1;
	uint_8_side_channel	s_in;
	coeff_side_channel xcoeff_in;
	coeff_side_channel ycoeff_in;
	uint_64_side_channel s_out;

    static int channel_dict[4];
    switch(bayer_pattern) {
        case BAYER_BGGR: {channel_dict[0]=0;channel_dict[1]=1;channel_dict[2]=1;channel_dict[3]=2; break;}
        case BAYER_GRBG: {channel_dict[0]=1;channel_dict[1]=0;channel_dict[2]=2;channel_dict[3]=1; break;}
        case BAYER_GBRG: {channel_dict[0]=1;channel_dict[1]=2;channel_dict[2]=0;channel_dict[3]=1; break;}
        case BAYER_RGGB: {channel_dict[0]=2;channel_dict[1]=1;channel_dict[2]=1;channel_dict[3]=0; break;}
        default:{channel_dict[0]=0;channel_dict[1]=1;channel_dict[2]=1;channel_dict[3]=2; break;}
    }

    int out_of_bound_rows = 0;
    int i = 0;
    while (row_count < row - 1) {
		i=0;
		LOAD_ROW_LOOP: while (i < col) {
#pragma HLS pipeline
			// Reading in a pixel from stream
			s_in = inStream.read();
			row_buffer[i] = s_in.data;
			i++;
		}
		row_count++;

		for (int i = 0; i < POLY_DEGREE; i++) {
#pragma HLS pipeline
	    	float_convert x;
	    	float_convert y;
			x.i = xcoeffStream.read();
			y.i = ycoeffStream.read();
			xcoeffs[i] = x.f;
			ycoeffs[i] = y.f;
		}

	    for (int i =0; i < MAX_VERTICAL_REMAP_DIFF; i++) {
#pragma HLS unroll
	    	how_many_pixels_landed_here[i] = 0;
	    }

		// Splatty on row buffer
        EACH_ROW_LOOP: for(int xin = 0; xin < HALF_COL; xin++) {
#pragma HLS pipeline
#pragma HLS dependence variable=output_buffer intra false
#pragma HLS dependence variable=secondary_buffer2 intra false

        	forward_loc outpoints1 = LUT_fwdf2point(row_count, xin, xcoeffs, ycoeffs);
            float x1 = outpoints1.x;
            float y1 = outpoints1.y;

            int xin2 = xin+HALF_COL;
        	forward_loc outpoints2 = LUT_fwdf2point(row_count, xin2, xcoeffs, ycoeffs);
            float x2 = outpoints2.x;
            float y2 = outpoints2.y;

            int channel1 = channel_dict[(1-xin%2)*2 + (1-row_count%2)];
            int channel2 = channel_dict[(1-xin2%2)*2 + (1-row_count%2)];

            INTENSITY intensity1 = row_buffer[xin];
            INTENSITY intensity2 = row_buffer[xin2];

            neighbour nbrs1[NUM_NEIGHBOURS];
            neighbour nbrs2[NUM_NEIGHBOURS];

 			n_nearest_neighbours(nbrs1, y1, x1);
			n_nearest_neighbours(nbrs2, y2, x2);

            SPLAT_PIXEL_LOOP: for (int nbrs_idx=0; nbrs_idx < NUM_NEIGHBOURS; nbrs_idx++) {
            	int xn1 = nbrs1[nbrs_idx].y;
				int yn1 = nbrs1[nbrs_idx].x;
                if (yn1>=miny && yn1<=(miny+op_buffer_width) && yn1<row && xn1>=0 && xn1<col) {
                	int y1_coor = yn1%op_buffer_width;
                    how_many_pixels_landed_here[y1_coor]+=1;
                    float w=hls::exp(-1 * (nbrs1[nbrs_idx].val * nbrs1[nbrs_idx].val));
                    output_buffer[channel1][yn1%op_buffer_width][xn1] += intensity1*w;
                    secondary_buffer2[channel1][yn1%op_buffer_width][xn1] += w;
                }

				int xn2 = nbrs2[nbrs_idx].y;
				int yn2 = nbrs2[nbrs_idx].x;
				if (yn2>=miny && yn2<=(miny+op_buffer_width) && yn2<row && xn2>=0 && xn2<col) {
					int y2_coor = yn2%op_buffer_width;
					how_many_pixels_landed_here[y2_coor]+=1;
					float w=hls::exp(-1 * (nbrs2[nbrs_idx].val * nbrs2[nbrs_idx].val));
					output_buffer[channel2][yn2%op_buffer_width][xn2] += intensity2*w;
					secondary_buffer2[channel2][yn2%op_buffer_width][xn2] += w;
				}
            }
        }

        if (how_many_pixels_landed_here[miny%op_buffer_width]==0) {
			int ycoord=miny % op_buffer_width;

			OUT_LOOP: for (i = 0; i < col; i+=2) {
#pragma HLS pipeline II = 3

				uint64_t output[3][2];
#pragma HLS array_partition variable=output complete

				for(int j=0; j<shape_1; j++){
#pragma HLS unroll
					for (int k = 0; k < 2; k++) {
						if (secondary_buffer2[j][ycoord][i+k]== 0.0) {
							output[j][k] = 0.0;
						} else {
							output[j][k] = (uint8_t) (output_buffer[j][ycoord][i+k] / secondary_buffer2[j][ycoord][i+k]);
						}
						output_buffer[j][ycoord][i+k]=0.0;
						secondary_buffer2[j][ycoord][i+k]=0.0;
					}
				}

				// Output pixel to stream BGR to GBR
				s_out.data =  output[2][1] << 40 | output[0][1] << 32 | output[1][1] << 24 | output[2][0] << 16 | output[0][0] << 8 | output[1][0];
				s_out.keep = s_in.keep;
				s_out.strb = s_in.strb;
				s_out.user = s_in.user;

				if (i == col - 1 && row_count == row-1) {
					s_out.last = 1;
				} else {
					s_out.last = 0;
				}

				s_out.id = s_in.id;
				s_out.dest = s_in.dest;
				outStream.write(s_out);
			}

			how_many_pixels_landed_here[miny%op_buffer_width]=0;
			miny++;
		}
        if (row_count == IMG_HEIGHT_OR_ROWS - 1) {
        	while(miny < IMG_HEIGHT_OR_ROWS) {
				int ycoord=miny % op_buffer_width;
				for (i = 0; i < col; i+=2) {
				#pragma HLS pipeline II = 3

					uint64_t output[3][2];
					#pragma HLS array_partition variable=output complete

					for(int j=0; j<shape_1; j++) {
					#pragma HLS unroll
						for (int k = 0; k < 2; k++) {
							if (secondary_buffer2[j][ycoord][i+k]== 0.0) {
								output[j][k] = 0.0;
							} else {
								output[j][k] = (uint8_t) (output_buffer[j][ycoord][i+k] / secondary_buffer2[j][ycoord][i+k]);
							}
							output_buffer[j][ycoord][i+k]=0.0;
							secondary_buffer2[j][ycoord][i+k]=0.0;
						}
					}

					// Output pixel to stream BGR to GBR
					s_out.data =  output[2][1] << 40 | output[0][1] << 32 | output[1][1] << 24 | output[2][0] << 16 | output[0][0] << 8 | output[1][0];
					s_out.keep = s_in.keep;
					s_out.strb = s_in.strb;
					s_out.user = s_in.user;

					if (i == col - 1 && row_count == row-1) {
						s_out.last = 1;
					} else {
						s_out.last = 0;
					}

					s_out.id = s_in.id;
					s_out.dest = s_in.dest;
					outStream.write(s_out);
				}
				how_many_pixels_landed_here[miny%op_buffer_width]=0;
				miny++;
			}
        }
    }
}
