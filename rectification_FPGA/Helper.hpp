#include "hls_math.h"

#define NUM_NEIGHBOURS 8
#define POLY_DEGREE 8
#define XCOEFF_SIZE 512
#define YCOEFF_SIZE 512

typedef struct neighbour {
   float val;
   int	x;
   int  y;
} neighbour;

typedef struct forward_loc {
   float x;
   float y;
} forward_loc;

typedef union {
	uint32_t i;
	float f;
} float_convert;

float distance(float x1,float y1,float x2,float y2);
void n_nearest_neighbours(neighbour result[NUM_NEIGHBOURS], float x, float y);
forward_loc LUT_fwdf2point(int i_in,int j_in, float xcoeffs[POLY_DEGREE], float ycoeffs[POLY_DEGREE]);
