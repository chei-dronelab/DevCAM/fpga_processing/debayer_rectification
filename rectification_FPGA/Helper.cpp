#include "Helper.hpp"

float distance(float x1, float y1, float x2, float y2) {
    return hls::abs(x1-x2) + hls::abs(y1-y2);
}

void n_nearest_neighbours(neighbour result[NUM_NEIGHBOURS], float x, float y) {
    int xint = int(x), yint = int(y);
    float fracx = float(xint)>x?(1-float(xint)+x):(x-float(xint));
    float fracy = float(yint)>y?(1-float(yint)+y):(y-float(yint));
    int window_size=int(ceil(hls::sqrt(NUM_NEIGHBOURS)));
    int w2 = window_size/2;
    int xmin = (xint-w2), xmax = (xint+w2);
    int ymin = (yint-w2), ymax = (yint+w2);
    int total_count = 0;

    if(window_size%2==0){
        if(fracx<0.5){
            xmin++;
        }
        else{
            xmax--;
        }
        if(fracy<0.5){
            ymin++;
        }
        else{
            ymax--;
        }
    }

    for(int xn=0; xn<3; xn++){
#pragma HLS pipeline
#pragma HLS unroll
        for(int yn=0; yn<3; yn++){
            float a=distance(x,y,xmin+xn,ymin+yn);
            result[total_count].val = a;
            result[total_count].x = xmin+xn;
            result[total_count].y = ymin+yn;
            total_count++;
        }
    }

}

forward_loc LUT_fwdf2point(int i_in,int j_in, float xcoeffs[POLY_DEGREE], float ycoeffs[POLY_DEGREE]) {

	float xvar=1.0;
	float yvar=1.0;
	float xval=0.0;
	float yval=0.0;

	for(int x=POLY_DEGREE-1;x>=0;x--) {
#pragma HLS pipeline
#pragma HLS unroll
        xval+=xcoeffs[x]*xvar;
        yval+=ycoeffs[x]*xvar;
        xvar*=j_in;
	}

	forward_loc result;
	result.x = xval;
	result.y = yval;
    return result;
}

