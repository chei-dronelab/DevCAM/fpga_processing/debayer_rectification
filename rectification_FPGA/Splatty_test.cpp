// Reference:
// https://forums.xilinx.com/t5/High-Level-Synthesis-HLS/AXIvideo2Mat-Descrepency/td-p/936133
// https://www.youtube.com/watch?v=UobM_XgBdEg&list=PLo7bVbJhQ6qzK6ELKCm8H_WEzzcr5YXHC&index=15

#include <fstream>
#include <string>
#include <opencv2/core/core.hpp>
#include <hls_opencv.h>
#include "Splatty_top.hpp"


// Image file path
#define	INPUT_IMAGE_CORE  	"C:/Users/hax032/Desktop/Splatty_Hardware_Impl/test.png"
//#define	INPUT_IMAGE_CORE  	"G:/My Drive/Research/Dronelab_Research/debayer_rectification/rectification_c++/imgs/input/iR.jpg"
#define INPUT_IMAGE_REF		"G:/My Drive/Research/Dronelab_Research/debayer_rectification/rectification_c++/imgs/output/test_output_stream_Right_cam_LUT_compressed.png"
#define OUTPUT_IMAGE_CORE 	"C:/Users/hax032/Desktop/Splatty_Hardware_Impl/FilterResult.png"

// Limited for error
#define LIMIT 0

void saveImage(const std::string path, cv::InputArray inArr) {
	double min;
	double max;
	cv::minMaxIdx(inArr, &min, &max);
	cv::Mat adjMap;
	cv::convertScaleAbs(inArr, adjMap, 255 / max);
	cv::imwrite(path,adjMap);
}

uint8_t inImage[IMG_HEIGHT_OR_ROWS][IMG_WIDTH_OR_COLS];
BGR_PIXEL outImage[IMG_HEIGHT_OR_ROWS][IMG_WIDTH_OR_COLS];

int main() {
	// Read input image
	printf("Load image %s\n",INPUT_IMAGE_CORE);
	cv::Mat imageSrc, imageRef;
	imageSrc = cv::imread(INPUT_IMAGE_CORE, cv::IMREAD_COLOR);
	imageRef = cv::imread(INPUT_IMAGE_REF, cv::IMREAD_COLOR);
	cv::Mat imgCvSrc_bayer(cv::Size(imageSrc.cols, imageSrc.rows), CV_8UC1, inImage);

	int channel[] = {0,1,1,2};
    for(int y=0;y<imageSrc.rows ;y++){
        for(int x=0;x<imageSrc.cols;x++){
            int c = channel[(1-x%2)*2 + (1-y%2)];
            imgCvSrc_bayer.at<uint8_t>(y,x) = imageSrc.at<BGR_PIXEL>(y, x).val[c];
        }
    }

	// Load in Image Coefficient
	printf("Loading Coeffs......\n");
	float xcoeffs[XCOEFF_SIZE][POLY_DEGREE], ycoeffs[YCOEFF_SIZE][POLY_DEGREE];
	std::fstream x_file;
	std::fstream y_file;

	x_file.open("C:/Users/hax032/Desktop/Splatty_Hardware_Impl/kitti_02_cmp_mapx_jh2.txt");
	y_file.open("C:/Users/hax032/Desktop/Splatty_Hardware_Impl/kitti_02_cmp_mapy_jh2.txt");
	//x_file.open("G:/My Drive/Research/Dronelab_Research/debayer_rectification/rectification_c++/data/compressed_LUT_Rx.txt");
	//y_file.open("G:/My Drive/Research/Dronelab_Research/debayer_rectification/rectification_c++/data/compressed_LUT_Ry.txt");
	//x_file.open("C:/Users/hax032/Desktop/Splatty_Hardware_Impl/compressed_identity_LUT_x.txt");
	//y_file.open("C:/Users/hax032/Desktop/Splatty_Hardware_Impl/compressed_identity_LUT_y.txt");

	std::string line;
	float temp;
	int i = 0;
	int j = 0;

    while (i < XCOEFF_SIZE) {
    	x_file >> line;
    	temp = stof(line);
    	xcoeffs[i][j] = temp;
    	if (j == POLY_DEGREE-1) {
    		j = 0;
    		i++;
    	} else {
    		j++;
    	}
    }

    i = 0; j = 0;
    while (i < YCOEFF_SIZE) {
    	y_file >> line;
    	temp = stof(line);
    	ycoeffs[i][j] = temp;
    	if (j == POLY_DEGREE-1) {
    		j = 0;
    		i++;
    	} else {
    		j++;
    	}
    }
	printf("Coeffs Array Loaded\n");

	// Define streams for input and output
	hls::stream<uint_8_side_channel> inputStream;
	hls::stream<uint_64_side_channel> outputStream;

	printf("Input Image Rows:%d Cols:%d\n",imageSrc.rows, imageSrc.cols);

	// OpenCV mat that point to a array (cv::Size(Width, Height))
	cv::Mat imgCvOut(cv::Size(imageSrc.cols, imageSrc.rows), CV_8UC3, outImage);

	bool pass = true;
	uint_8_side_channel valIn;
	uint_64_side_channel valOut;

	for(int i = 0; i < imgCvSrc_bayer.rows; i++) {
	    for(int j = 0; j < imgCvSrc_bayer.cols; j++) {
	    	INTENSITY pixel = imgCvSrc_bayer.at<INTENSITY>(i, j);
	    	valIn.data = pixel;
	    	valIn.keep = 1; valIn.strb = 1; valIn.user = 1; valIn.id = 0; valIn.dest = 0;
	    	if (i == imgCvSrc_bayer.rows-1 && j == imgCvSrc_bayer.cols-1) {
	    		valIn.last = 1;
	    	} else {
	    		valIn.last = 0;
	    	}
	    	inputStream << valIn;
	    }
	}

	hls::stream<coeff_side_channel> xcoeff_stream;
	hls::stream<coeff_side_channel> ycoeff_stream;
	coeff_side_channel xcoeff_in;
	coeff_side_channel ycoeff_in;

	for(int i = 0; i < YCOEFF_SIZE; i++) {
	    for(int j = 0; j < POLY_DEGREE; j++) {
	    	float_convert x;
	    	float_convert y;

	    	x.f = xcoeffs[i][j];
	    	y.f = ycoeffs[i][j];

	    	xcoeff_stream << x.i;
	    	ycoeff_stream << y.i;
	    }
	}
	printf("Coeffs Stream Loaded\n");

	printf("Splatty Running......\n");
    Splatty_top(inputStream, xcoeff_stream, ycoeff_stream, outputStream, BAYER::BAYER_BGGR);

    printf("Checking Results......\n");
	for (int idxRows=0; idxRows < imageSrc.rows; idxRows++) {
		// Test the row
		for (int idxCols=0; idxCols < imageSrc.cols; idxCols+=2) {
			outputStream.read(valOut);

			//GBR to BGR
			BGR_PIXEL pixel1, pixel2, refPixel1, refPixel2;
			pixel1.val[0] = valOut.data << 48 >> 56;
			pixel1.val[1] = valOut.data << 56 >> 56;
			pixel1.val[2] = valOut.data << 40 >> 56;

			pixel2.val[0] = valOut.data << 24 >> 56;
			pixel2.val[1] = valOut.data << 32 >> 56;
			pixel2.val[2] = valOut.data << 16 >> 56;

			refPixel1 = imageRef.at<BGR_PIXEL>(idxRows, idxCols);
			refPixel2 = imageRef.at<BGR_PIXEL>(idxRows, idxCols+1);

			imgCvOut.at<BGR_PIXEL>(idxRows, idxCols) = pixel1;
			imgCvOut.at<BGR_PIXEL>(idxRows, idxCols+1) = pixel2;
		}
	}

	if (pass) {
		printf("Passed! Saving image\n");
		saveImage(std::string(OUTPUT_IMAGE_CORE), imgCvOut);
	}

	return 0;
}

