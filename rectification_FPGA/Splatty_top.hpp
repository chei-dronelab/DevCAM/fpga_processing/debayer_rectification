#include <stdio.h>
#include <stdlib.h>
#include <ap_axi_sdata.h>
#include <hls_video.h>
#include <float.h>
#include "Helper.hpp"

#define IMG_WIDTH_OR_COLS 1392
#define IMG_HEIGHT_OR_ROWS 512
#define HALF_COL 696

#define MAX_VERTICAL_REMAP_DIFF 60

typedef ap_axiu<64,1,1,1> uint_64_side_channel;
typedef ap_axiu<8,1,1,1> uint_8_side_channel;
typedef uint32_t coeff_side_channel;

typedef hls::Scalar<3, uint8_t> BGR_PIXEL;
typedef uint8_t INTENSITY;

enum BAYER {BAYER_BGGR, BAYER_GRBG, BAYER_GBRG, BAYER_RGGB};

void Splatty_top(hls::stream<uint_8_side_channel> &inStream, hls::stream<coeff_side_channel> &xcoeffStream,
				hls::stream<coeff_side_channel> &ycoeffStream, hls::stream<uint_64_side_channel> &outStream, BAYER bayer_pattern);
